var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var nameSchema = new Schema({
    invoiceno: {
        type: Number,
        trim: true,
        default: null 
    },
    name: {
        type: String,
        trim: true,
        default: null 
    },
    dob: {
        type: String,
        trim: true,
        default: null 
    },
    age: {
        type: String,
        trim: true,
        default: null 
    },
    email: {
        type: String,
        trim: true,
        default: null
    },
    mobile: {
        type: String,
        trim: true,
        default: null
    },
    addressline: {
        type: String,
        trim: true,
        default: null
    },
    addressline1: {
        type: String,
        trim: true,
        default: null 
    },
    City: {
        type: String,
        trim: true,
        default: null 
    },
    State: {
        type: String,
        trim: true,
        default: null 
    },
    Country: {
        type: String,
        trim: true,
        default: null 
    },
    Zip: {
        type: String,
        trim: true,
        default: null 
    },
    fileUpload: [],
    PassedCompanyDetails: [],
    EducationDetails: [],
    OtherCertification: []
});

var PostData = mongoose.model("PostData", nameSchema);

module.exports = {
    PostData: PostData,
}

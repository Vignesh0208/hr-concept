var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var passportLocalMongoose = require("passport-local-mongoose");

var UserSchema = new Schema({
    username: {
        type: String,
        trim: true,
        default: null
    },
    password: {
        type: String,
        trim: true,
        default: null
    }
});

UserSchema.plugin(passportLocalMongoose);
const User = mongoose.model("User", UserSchema);

module.exports = {
    User: User,
}

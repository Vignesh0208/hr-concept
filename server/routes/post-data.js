const express = require('express');
const router = express.Router();
const PostData = require('../models/post-data').PostData

router.post('/', (req, res, next) => {
    var user = new PostData({});
    user.invoiceno= req.body.invoiceno
    user.name= req.body.name
    user.dob= req.body.dob
    user.age= req.body.age
    user.email= req.body.email
    user.mobile= req.body.mobile
    user.addressline= req.body.addressline
    user.addressline1= req.body.addressline1
    user.City= req.body.City
    user.State= req.body.State
    user.Country= req.body.Country
    user.Zip= req.body.Zip
    user.fileUpload= req.body.fileUpload
    user.PassedCompanyDetails= req.body.PassedCompanyDetails
    user.EducationDetails= req.body.EducationDetails
    user.OtherCertification= req.body.OtherCertification

    user.save().then(item => {
        res.redirect('/');
    })
})

module.exports = router;
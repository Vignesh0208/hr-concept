const express = require('express');
const router = express.Router();
const { Parser, transforms: { unwind } } = require('json2csv');
const PostData = require('../models/post-data').PostData

router.get('/', (req, res) => {
    PostData.find().then(function(result){
        res.render('index.html', {
            title: 'Printable Invocie',
            className: 'home-page',
            result: JSON.stringify(result)
        })
   });
});

router.get('/convert-csv', (req, res) => {
    PostData.find().then(function(result) {
        var dataNew = []
        
        for(var i=0; i < result.length; i++) {
            var val = {
                "invoiceno": result[i].invoiceno,
                "name": result[i].name,
                "dob": result[i].dob,
                "age": result[i].age,
                "email": result[i].email,
                "mobile": result[i].mobile,
                "addressline": result[i].addressline,
                "addressline1": result[i].addressline1,
                "City": result[i].City,
                "State": result[i].State,
                "Country": result[i].Country,
                "Zip": result[i].Zip,
                "PassedCompanyDetails": [],
                "EducationDetails": [],
                "OtherCertification": []
            }
            for (var j = 0; j < result[i].PassedCompanyDetails.length; j++) {
                var pcd = {
                    "jobTitle": result[i].PassedCompanyDetails[j].jobTitle,
                    "employer": result[i].PassedCompanyDetails[j].employer,
                    "startDate": result[i].PassedCompanyDetails[j].startDate,
                    "endDate": result[i].PassedCompanyDetails[j].endDate,
                    "responsibilities": result[i].PassedCompanyDetails[j].responsibilities,
                    "awardsRecognition": result[i].PassedCompanyDetails[j].awardsRecognition
                }
                val.PassedCompanyDetails.push(pcd);
            }
            for (var j = 0; j < result[i].EducationDetails.length; j++) {
                var ed = {
                    "schoolName": result[i].EducationDetails[j].schoolName,
                    "course": result[i].EducationDetails[j].course,
                    "specialisation": result[i].EducationDetails[j].specialisation,
                    "percentage": result[i].EducationDetails[j].percentage
                }
                val.EducationDetails.push(ed);
            }
            for (var j = 0; j < result[i].OtherCertification.length; j++) {
                var oc = {
                    "institute": result[i].OtherCertification[j].institute,
                    "certification": result[i].OtherCertification[j].certification,
                    "year": result[i].OtherCertification[j].year
                }
                val.OtherCertification.push(oc);
            }
            dataNew.push(val);
        }
        const fields = ['invoiceno', 'name', 'dob', 'age', 'email', 'mobile', 'addressline', 'addressline1', 'City', 'State', 'Country', 'Zip', 'PassedCompanyDetails.jobTitle', 'PassedCompanyDetails.employer', 'PassedCompanyDetails.startDate', 'PassedCompanyDetails.endDate', 'PassedCompanyDetails.responsibilities', 'PassedCompanyDetails.awardsRecognition', 'EducationDetails.schoolName', 'EducationDetails.course', 'EducationDetails.specialisation', 'EducationDetails.percentage', 'OtherCertification.institute', 'OtherCertification.certification', 'OtherCertification.year'];
        const transforms = [unwind({ paths: ['PassedCompanyDetails', 'EducationDetails', 'OtherCertification'] })];
        const json2csvParser = new Parser({ fields, transforms });
        const csv = json2csvParser.parse(dataNew);
        
        res.json({csvoutput: csv})
    });
});

router.get('/view/:id', (req, res) => {
    PostData.findOne({"invoiceno": req.params.id})
    .then(function(result) {
        res.render('pages/view.html', {
            title: 'Printable Invocie',
            className: 'print-page-view',
            user: result
        })
   });
});

router.get('/edit/:id', (req, res) => {
    PostData.findOne({"invoiceno": req.params.id})
    .then(function(result) {
        res.render('pages/edit.html', {
            title: 'Printable Invocie',
            className: 'edit-page',
            result: JSON.stringify(result),
            user: result
        })
   });
});

router.post('/edit/:id', (req, res) => {
    PostData.updateOne({"invoiceno": req.params.id}, { $set: {
        "clientname": req.body.clientname,
        "symbol": req.body.symbol,
        "gstnumber": req.body.gstnumber,
        "pannumber": req.body.pannumber,
        "clientaddress": req.body.clientaddress,
        "clientaddress1": req.body.clientaddress1,
        "City": req.body.City,
        "State": req.body.State,
        "Country": req.body.Country,
        "Zip": req.body.Zip,
        "invoicedate": req.body.invoicedate,
        "billingperiod": req.body.billingperiod,
        "subtotal": req.body.subtotal,
        "gstprice": req.body.gstprice,
        "total": req.body.total,
        "tds": req.body.tds,
        "balancedue": req.body.balancedue,
        "fullValue": req.body.fullValue,
        "igstper": req.body.igstper,
        "tdsper": req.body.tdsper
    }}).exec((err) => {
        if(err) return err;
        return res.json({ success: 'okay' })
    })
})

module.exports = router;
const express = require('express');
const router = express.Router();

var dummyData = ''
var value = ''

router.post('/', (req, res, next) => {
    dummyData= req.body;
    value = JSON.stringify(req.body);
    res.json(req.body);
})

router.get('/', (req, res) => {
    res.render('pages/print-page.html', {
        title: 'Printable Invocie',
        className: 'print-page-view',
        user: dummyData,
        value: value
    })
});

module.exports = router;
const express = require('express');
const router = express.Router();
const PostData = require('../models/post-data').PostData

router.get('/', (req, res) => {
    PostData.find().then(function(result){
        res.render('pages/new-detail.html', {
            title: 'Printable Invocie',
            className: 'new-detail',
            values: JSON.stringify(result)
        })
   });
});

module.exports = router;
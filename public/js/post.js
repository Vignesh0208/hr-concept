var countryId= 'India';
var fullObject= '';
var state = '';
var givedata= '';
var startDate;

// Enter only number
$(".number").bind("keypress", (e) => {
    var keyCode = e.which ? e.which : e.keyCode;
    return keyCode >= 48 && keyCode <= 57 ? true : !isNaN(Number(e.key))
});

// Multiple date picker
$(function() {
    $('.billingperiod').daterangepicker({
        "maxDate": moment().format("DD/MM/YYYY"),
        "drops": "down",
        "opens": 'left',
        locale: {
            format: 'DD/MM/YYYY'
        }
    });
});

// Single date picker
$(function() {
    $('.dob').daterangepicker({
        autoApply: true,
        maxDate: moment().format("DD/MM/YYYY"),
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'),10),
        locale: {
            format: 'DD/MM/YYYY'
        },
    }, function(start, end, label) {
        var years = moment().diff(start, 'years');
        $(".age").html(years);
    });
});

// $('#startDate').change(function() {
//     this.startDate = $(this).val();
//     updateDate(this.startDate);
// })

function updateDate() {
    // Start date picker
    $('#DetailsCompany .startDate').each(function() {
        $(this).datepicker({
            format: "mm/yyyy",
            startView: "months", 
            minViewMode: "months",
            endDate: "today",
            autoclose: true
        })
    });
    // End date picker
    $('#DetailsCompany .endDate').each(function(){
        $(this).datepicker({
            format: "mm/yyyy",
            startView: "months", 
            minViewMode: "months",
            endDate: "today",
            autoclose: true
        })
    });
}

// Update country list in section option
function countries(data) {
    for(i = 0; i < data.length; i++) {
        $(".countries").append(`
            <option value="`+ i +`">`+ data[i].name +`</option>
        `)
    }
}

// Update states list in section option
$(".countries").on('change', function(el) {
    state = fullObject[$(el.target).val()].states;
    var sample = Object.getOwnPropertyNames(fullObject[$(el.target).val()].states);
    states(sample)
});

function states(data) {
    $(".states").empty();
    for(i = 0; i < data.length; i++) {
        $(".states").append(`
            <option value="`+ data[i] +`">`+ data[i] +`</option>
        `)
    }
}

// Update city list in section option
$(".states").on('change', function(el) {
    var sample = state[$(el.target).val()];
    city(sample);
});

function city(data) {
    $(".cities").empty();
    for(i = 0; i < data.length; i++) {
        $(".cities").append(`
            <option value="`+ data[i] +`">`+ data[i] +`</option>
        `)
    }
}

function removeEdu(deleteEdu) {
    if(deleteEdu == 0) {
        $(".removeEdu").hide();
    }
    else {
        $(".removeEdu").show();
    }
}


$(document).ready(() => {
    updateDate();

    $('#checkbox-endDate').change(function() {
        if($(this).is(":checked")) {
            $(".endDate").attr('disabled','disabled');
        }
        else {
            $(".endDate").removeAttr('disabled');
        }
    });

    // Get countries states cities from json
    var url = "/public/json/countries-states-cities.json";
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function(data)
        {
            fullObject= data
            countries(data);
        }
    });

    var deleteEdu = $(".deleteEducation").length - 1;
    removeEdu(deleteEdu);

    // Delete detail
    $("#addEducation").on('click', '.removeEdu', function(e) {
        if (deleteEdu != 0) {
            $(this).parents(".detailEdu").remove();
            deleteEdu --;
            removeEdu(deleteEdu)
        }
    })

    // Add detail
    $("#addnewEducation").click(() => {
        $(".detailEdu:last").after(`
        <tr class="detailEdu">
            <td>
                <input type="text" class="form-control detailValue schoolName" id="schoolName" placeholder="School or college name" />
            </td>
            <td>
                <input type="text" class="form-control detailValue course" id="course" placeholder="Course">
            </td>
            <td>
                <input type="text" class="form-control specialisation detailValue" id="specialisation" placeholder="Specialisation" />
            </td>
            <td class="deleteEducation">
                <div class="position-relative">
                    <input type="text" class="form-control detailValue percentage number" id="percentage" placeholder="Percentage" />
                    <!-- <div id="price" class="detailValue text-center price">0</div> -->
                    <div class="removeEdu cursor-pointer position-absolute">
                        <img src="/public/images/delete.svg" />
                    </div>
                </div>
            </td>
        </tr>
        `);
        deleteEdu ++;
        removeEdu(deleteEdu)
    });

    function removeCompany(deleteCompany) {
        if(deleteCompany == 0) {
            $(".removeCompany").hide();
        }
        else {
            $(".removeCompany").show();
        }
    }

    var deleteCompany = $(".last-company").length - 1;
    removeCompany(deleteCompany);

    // Delete detail
    $("#addDetailsCompany").on('click', '.removeCompany', function(e) {
        if (deleteCompany != 0) {
            $(this).parents(".last-company").remove();
            deleteCompany --;
            removeCompany(deleteCompany)
        }
    })

    // Add detail
    $("#addnewCompany").click(() => {
        $(".last-company:last").after(`
        <div class="last-company position-relative" id="DetailsCompany">
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="jobTitle">Job Title</label>
                    <input type="text" class="form-control jobTitle" id="jobTitle" name="jobTitle">
                </div>
                <div class="form-group col-md-6">
                    <label for="employer">Employer</label>
                    <input type="text" class="form-control employer" id="employer" name="employer">
                </div>
                <div class="form-group col-md-6">
                    <label for="startDate">Start Date</label>
                    <div style="position: relative;">
                        <input type="text" name="startDate" class="form-control startDate" id="startDate" value="09/2020"><i class="far fa-calendar-alt"></i>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="checkbox-endDate">End Date
                        <input type="checkbox" id="checkbox-endDate">
                        <span class="checkmark"></span>
                    </label>
                    <div style="position: relative;">
                        <input type="text" name="endDate" class="form-control endDate" id="endDate"  value="09/2020"><i class="far fa-calendar-alt"></i>
                    </div>
                </div>
                <div class="mb-0 form-group col-md-6">
                    <label for="responsibilities">Description</label>
                    <textarea type="text" name="responsibilities" class="form-control responsibilities" id="responsibilities" placeholder="Description"></textarea>
                </div>
                <div class="mb-0 form-group col-md-6">
                    <label for="awardsRecognition">Awards & Recognition</label>
                    <textarea type="text" name="awardsRecognition" class="form-control awardsRecognition" id="awardsRecognition" placeholder="Awards & Recognition"></textarea>
                </div>
                <div class="deleteCompany">
                    <div class="removeCompany cursor-pointer position-absolute">
                        <img src="/public/images/delete.svg" />
                    </div>
                </div>
            </div>
        </div>
        `);
        deleteCompany ++;
        removeCompany(deleteCompany)
        updateDate();
    });

    function removeCft(deleteCtf) {
        if(deleteCtf == 0) {
            $(".removeCtf").hide();
        }
        else {
            $(".removeCtf").show();
        }
    }

    var deleteCtf = $(".deleteCertification").length - 1;
    removeCft(deleteCtf);

    // Delete detail
    $("#addCertification").on('click', '.removeCtf', function(e) {
        if (deleteCtf != 0) {
            $(this).parents(".detailCertification").remove();
            deleteCtf --;
            removeCft(deleteCtf)
        }
    })

    // Add detail
    $("#addnewCertification").click(() => {
        $(".detailCertification:last").after(`
        <tr class="detailCertification">
            <td>
                <input type="text" class="form-control detailValue institute" id="institute" placeholder="Institute name" />
            </td>
            <td>
                <input type="text" class="form-control detailValue certification" id="certification " placeholder="Certification">
            </td>
            <td class="deleteCertification">
                <div class="position-relative">
                    <input type="text" class="form-control detailValue year number" id="year" placeholder="Year" />
                    <!-- <div id="price" class="detailValue text-center price">0</div> -->
                    <div class="removeCtf cursor-pointer position-absolute">
                        <img src="/public/images/delete.svg" />
                    </div>
                </div>
            </td>
        </tr>
        `);
        deleteCtf ++;
        removeCft(deleteCtf)
    });
})
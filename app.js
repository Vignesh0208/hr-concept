var config = require('./config');
var express = require ('express');
var multer  = require('multer');
var nunjucks  = require('nunjucks');
var app = express();
const mongoose = require('mongoose');
const cors = require('cors');
const compression = require('compression');
var bodyParser = require('body-parser');
const expressNunjucks = require('express-nunjucks');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var passport = require('passport');
const User = require('./server/models/user.js').User;

// BodyParser Middleware
app.use(bodyParser.urlencoded({limit: '50mb', extended: false }))
app.use(bodyParser.json({limit: '50mb'}))
app.use(cookieParser());

// Express Session
app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true
}));

// Passport init
app.use(passport.initialize());
app.use(passport.session());
passport.serializeUser(User.serializeUser()); 
passport.deserializeUser(User.deserializeUser()); 

const LocalStrategy = require('passport-local').Strategy; 
passport.use(new LocalStrategy(User.authenticate())); 

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*' );
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.use(cors());
app.use('/public', express.static('public'));
app.use(compression())
app.set('views', __dirname + '/views');

const mongoDB = config.db.mongo.address + config.db.mongo.dbname
mongoose.connect(mongoDB, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongoose.Promise = global.Promise;
var db = mongoose.connection;
mongoose.set('useFindAndModify', false);    
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

const njk = expressNunjucks(app, {
    watch: config.env,
    noCache: config.env
});


const IndexRoute = require('./server/routes/index')
const newDetail = require('./server/routes/new-detail')
const printPage = require('./server/routes/print-page')
const postData = require('./server/routes/post-data')
const deleteData = require('./server/routes/delete.js')
const login = require('./server/routes/login.js')

app.use('/', IndexRoute);
app.use('/new-detail', newDetail);
app.use('/print-page', printPage);
app.use('/post-data', postData);
app.use('/delete', deleteData);
app.use('/login', login);

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, 'uploads/');
    },
    filename: function (req, file, callback) {
        var mimetype= file.mimetype.split('/')[1];
        callback(null, file.fieldname + '-' + Date.now() + '.' + mimetype);
    }
});

var upload = multer({ storage : storage }).any('streamfile');

app.post('/fileupload',function(req,res){
    upload(req,res,function(err) {
        if(err) {
            return res.end("Error uploading file.");
        }
        res.send({filename: req.files});
    });
});

app.listen(config.port, function() {
    console.log('Application worker ' + config.port + ' started...');
});
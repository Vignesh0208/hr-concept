require('dotenv').config()

const config = {
    env: process.env.APP_ENV,
    port: process.env.WEB_PORT || 5000,
    session : {
        secretKey: process.env.SESSION_SECRET
    }
}

// if(config.env == 'development') {
    config.db = {}
    config.db.mongo = {
        address: 'mongodb://127.0.0.1:27017/',
        dbname:'hrConcept'
    }
// } else {
//     config.db = {}
//     config.db.mongo = {
//         address: 'mongodb://mongo:27017/',
//         dbname:'hrConcept'
//     }
// }

module.exports = config;
